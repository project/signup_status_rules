<?php
// $Id$
/**
 * @file
 *   Provides rules integration for the Signup Status module.
 */

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function signup_status_rules_rules_event_info() {
  return array(
    'signup_status_rules_status_new' => array(
      'label' => t('Signup status was created'),
      'module' => 'Signup Status',
      'arguments' => array(
        'signup' => array(
          'type' => 'unchanged_signup',
          'label' => t('signup data'),
          'handler' => 'signup_status_rules_events_argument_signup_data',
        ),
      ),
      'help' => 'This event is triggered when the signup status of an ' .
      'individual signup has been created by the Signup Status module.',
    ),
    'signup_status_rules_status_update' => array(
      'label' => t('Signup status was updated'),
      'module' => 'Signup Status',
      'arguments' => array(
        'signup' => array(
          'type' => 'signup',
          'label' => t('signup data'),
          'handler' => 'signup_status_rules_events_argument_signup_data',
        ),
      ),
      'help' => 'This event is triggered when the signup status of an ' .
      'individual signup has been changed by the Signup Status module.',
    ),
  );
}

/**
 * Event: Load associated user for a signup object
 */
function signup_status_rules_events_argument_signup_user($signup) {
  return user_load(array('uid' => $signup->uid));
}

/**
 * Event: Load the signup data
 */
function signup_status_rules_events_argument_signup_data($signup) {
  return $signup;
}

/**
 * Implementation of hook_rules_condition_info().
 * @ingroup rules
 */
function signup_status_rules_condition_info() {
  return array(
    'signup_status_rules_condition_signup_status_open' => array(
      'label' => t('Is signup status open for content'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
      ),
      'help' => t('Evaluates to TRUE if signups are open for the given node.')
      'module' => 'Signup',
    ),
    'signup_status_rules_condition_unchanged_status_has_value' => array(
      'label' => t('Unchanged signup status has value'),
      'arguments' => array(
        'unchanged_signup' => array('type' => 'unchanged_signup', 'label' => t('Unchanged signup status')),
      ),
      'help' => t('Evaluates to TRUE if the unchanged signup status has the selected value.')
      'module' => 'Signup Status',
    ),
    'signup_status_rules_condition_updated_status_has_value' => array(
      'label' => t('Updated signup status has value'),
      'arguments' => array(
        'updated_signup' => array('type' => 'signup', 'label' => t('Updated signup status')),
        ),
        'help' => t('Evaluates to TRUE if the updated signup status has the selected value.')
      'module' => 'Signup Status',
    ),
  );
}

/**
 * Condition: Unchanged signup status has value
 */
function signup_status_rules_argument_unchanged_status_has_value($signup, $settings) {
  return in_array($signup->old_status, $settings);
}

/**
 * Condition: Updated signup status has value
 */
function signup_status_rules_condition_updated_status_has_value($signup, $settings) {
  return in_array($signup->status, $settings);
}

/**
 * Condition: Signup status is open
 */
function signup_status_rules_condition_signup_status_open($signup) {
  $node = node_load($signup->nid);
  return $node->signup_status == 1;
}

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function signup_status_rules_action_info() {
  return array(
    'signup_status_rules_action_load_user' => array(
      'label' => t('Load signed up user'),
      'module' => 'Signup Status',
      'arguments' => array(
        'signup' => array('type' => 'signup', 'label' => t('Signup data')),
      ),
    ),
    'signup_status_rules_action_load_node' => array(
      'label' => t('Load the signup node'),
      'module' => 'Signup Status',
      'arguments' => array(
        'signup' => array('type' => 'signup', 'label' => t('Signup data')),
      ),
    ),
  );
}

/**
 * Action: Load the signed up user
 */
function signup_status_rules_action_load_user($signup) {
  if (!$signup->uid) {
    $account = new stdClass();
    $account->uid = 0;
    $account->name = '';
    $account->mail = $signup->anon_mail;
    $account->roles = array(DRUPAL_ANONYMOUS_RID => 'anonymous user');
  }
  else {
    $account = user_load($signup->uid);
  }
  return $account;
}

/**
 * Action: Load the node to which the user is signed up
 */
function signup_status_rules_action_load_node($signup) {
  return node_load($signup->nid);
}

/**
 * Implementation of hook_rules_data_type_info().
 * @ingroup rules
 */
function signup_status_rules_data_type_info() {
  return array(
    'signup' => array(
      'label' => t('Signup data'),
      'class' => 'rules_data_type_signup',
      'savable' => FALSE,
      'identifiable' => TRUE,
    ),
    'unchanged_signup' => array(
      'label' => t('Unchanged signup data'),
      'class' => 'rules_data_type_signup',
      'savable' => FALSE,
      'identifiable' => TRUE,
    ),
  );
}

/**
 * Implements the signup and unchanged signup data types.
 * @ingroup rules
 */
class rules_data_type_signup extends rules_data_type {
  function load($sid) {
    return signup_load_signup($sid);
  }

  function get_identifier() {
    $signup = $this->get();
    return $signup->sid;
  }
}
