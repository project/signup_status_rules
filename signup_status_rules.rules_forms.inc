<?php
// $Id$

/**
 * @file 
 *   Rules configuration forms for Signup Status Rules module
 *
 * @ingroup rules
 */

/**
 * Condition: Check for status configuration form
 */
function signup_status_rules_condition_unchanged_status_has_value_form($settings, &$form) {
  $statuses = signup_status_rules_get_statuses();
  foreach ($statuses as $sid => $status) {
    $options[$sid] = $status['name'];
  }
  $settings += array('status' => array());
  $form['settings']['status'] = array(
    '#type' => 'select',
    '#title' => t('Unchanged status'),
    '#options' => $options,
    '#multiple' => FALSE,
    '#default_value' => isset($settings['status']) ? $settings['status'] : array(),
    '#required' => TRUE,
  );
}

/**
 * Condition: Check for status configuration form
 */
function signup_status_rules_condition_updated_status_has_value_form($settings, &$form) {
  $statuses = signup_status_rules_get_statuses();
  foreach ($statuses as $sid => $status) {
    $options[$sid] = $status['name'];
  }
  $settings += array('status' => array());
  $form['settings']['status'] = array(
    '#type' => 'select',
    '#title' => t('Updated status'),
    '#options' => $options,
    '#multiple' => FALSE,
    '#default_value' => isset($settings['status']) ? $settings['status'] : array(),
    '#required' => TRUE,
  );
}

/**
 * Get available signup statuses
 */
function signup_status_rules_get_statuses() {
  static $statuses = array();
  if (empty($statuses)) {
    $result = db_query("SELECT * FROM {signup_status_codes} ORDER BY weight");
    while ($row = db_fetch_array($result)) {
      $statuses[$row['cid']] = $row;
    }
  }
  return $statuses;
}